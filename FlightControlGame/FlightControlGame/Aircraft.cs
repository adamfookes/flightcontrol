﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FlightControlGame
{
    class Aircraft
    {
        public int ID { get; set; }
        public double X { get; set; }
        public double Y { get; set; }
        public double Rotation { get; set; }
        public string Type { get; set; }
        public bool Landed { get; set; }
        public PictureBox pic = new PictureBox();
        public bool InsideMap = false;
    }
}
