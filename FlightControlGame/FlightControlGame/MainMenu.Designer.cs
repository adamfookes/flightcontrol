﻿namespace FlightControlGame
{
    partial class MainMenu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainMenu));
            this.OptionBtn = new System.Windows.Forms.Button();
            this.StatBtn = new System.Windows.Forms.Button();
            this.PlayBtn = new System.Windows.Forms.Button();
            this.NextBtn = new System.Windows.Forms.Button();
            this.Movement = new System.Windows.Forms.Timer(this.components);
            this.QuitBtn = new System.Windows.Forms.Button();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.HelpVideo = new AxWMPLib.AxWindowsMediaPlayer();
            this.Play = new System.Windows.Forms.Button();
            this.Pause = new System.Windows.Forms.Button();
            this.Fullscreen = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.HelpVideo)).BeginInit();
            this.SuspendLayout();
            // 
            // OptionBtn
            // 
            this.OptionBtn.BackColor = System.Drawing.Color.Transparent;
            this.OptionBtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.OptionBtn.FlatAppearance.BorderSize = 0;
            this.OptionBtn.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.OptionBtn.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.OptionBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.OptionBtn.ForeColor = System.Drawing.Color.Transparent;
            this.OptionBtn.Location = new System.Drawing.Point(851, 131);
            this.OptionBtn.Name = "OptionBtn";
            this.OptionBtn.Size = new System.Drawing.Size(63, 95);
            this.OptionBtn.TabIndex = 1;
            this.OptionBtn.TabStop = false;
            this.OptionBtn.UseVisualStyleBackColor = false;
            this.OptionBtn.Click += new System.EventHandler(this.OptionBtn_Click);
            // 
            // StatBtn
            // 
            this.StatBtn.BackColor = System.Drawing.Color.Transparent;
            this.StatBtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.StatBtn.FlatAppearance.BorderSize = 0;
            this.StatBtn.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.StatBtn.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.StatBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.StatBtn.ForeColor = System.Drawing.Color.Transparent;
            this.StatBtn.Location = new System.Drawing.Point(851, 234);
            this.StatBtn.Name = "StatBtn";
            this.StatBtn.Size = new System.Drawing.Size(63, 95);
            this.StatBtn.TabIndex = 2;
            this.StatBtn.TabStop = false;
            this.StatBtn.UseVisualStyleBackColor = false;
            this.StatBtn.Click += new System.EventHandler(this.StatBtn_Click);
            // 
            // PlayBtn
            // 
            this.PlayBtn.BackColor = System.Drawing.Color.Transparent;
            this.PlayBtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.PlayBtn.FlatAppearance.BorderSize = 0;
            this.PlayBtn.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.PlayBtn.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.PlayBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.PlayBtn.ForeColor = System.Drawing.Color.Transparent;
            this.PlayBtn.Location = new System.Drawing.Point(201, 192);
            this.PlayBtn.Name = "PlayBtn";
            this.PlayBtn.Size = new System.Drawing.Size(63, 43);
            this.PlayBtn.TabIndex = 3;
            this.PlayBtn.TabStop = false;
            this.PlayBtn.UseVisualStyleBackColor = false;
            this.PlayBtn.Click += new System.EventHandler(this.PlayBtn_Click);
            // 
            // NextBtn
            // 
            this.NextBtn.BackColor = System.Drawing.Color.Transparent;
            this.NextBtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.NextBtn.FlatAppearance.BorderSize = 0;
            this.NextBtn.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.NextBtn.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.NextBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.NextBtn.ForeColor = System.Drawing.Color.Transparent;
            this.NextBtn.Location = new System.Drawing.Point(370, 192);
            this.NextBtn.Name = "NextBtn";
            this.NextBtn.Size = new System.Drawing.Size(63, 43);
            this.NextBtn.TabIndex = 4;
            this.NextBtn.TabStop = false;
            this.NextBtn.UseVisualStyleBackColor = false;
            this.NextBtn.Click += new System.EventHandler(this.NextBtn_Click);
            // 
            // Movement
            // 
            this.Movement.Enabled = true;
            this.Movement.Tick += new System.EventHandler(this.Movement_Tick);
            // 
            // QuitBtn
            // 
            this.QuitBtn.BackColor = System.Drawing.Color.Transparent;
            this.QuitBtn.BackgroundImage = global::FlightControlGame.Properties.Resources.QuitBtn;
            this.QuitBtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.QuitBtn.FlatAppearance.BorderSize = 0;
            this.QuitBtn.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.QuitBtn.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.QuitBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.QuitBtn.Location = new System.Drawing.Point(12, 12);
            this.QuitBtn.Name = "QuitBtn";
            this.QuitBtn.Size = new System.Drawing.Size(70, 45);
            this.QuitBtn.TabIndex = 7;
            this.QuitBtn.TabStop = false;
            this.QuitBtn.UseVisualStyleBackColor = false;
            this.QuitBtn.Click += new System.EventHandler(this.QuitBtn_Click);
            // 
            // HelpVideo
            // 
            this.HelpVideo.Enabled = true;
            this.HelpVideo.Location = new System.Drawing.Point(578, 102);
            this.HelpVideo.Name = "HelpVideo";
            this.HelpVideo.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("HelpVideo.OcxState")));
            this.HelpVideo.Size = new System.Drawing.Size(222, 133);
            this.HelpVideo.TabIndex = 8;
            // 
            // Play
            // 
            this.Play.BackColor = System.Drawing.Color.Transparent;
            this.Play.BackgroundImage = global::FlightControlGame.Properties.Resources.PlayBtn;
            this.Play.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Play.FlatAppearance.BorderSize = 0;
            this.Play.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Play.Location = new System.Drawing.Point(578, 241);
            this.Play.Name = "Play";
            this.Play.Size = new System.Drawing.Size(70, 47);
            this.Play.TabIndex = 9;
            this.Play.UseVisualStyleBackColor = false;
            this.Play.Click += new System.EventHandler(this.Play_Click);
            // 
            // Pause
            // 
            this.Pause.BackColor = System.Drawing.Color.Transparent;
            this.Pause.BackgroundImage = global::FlightControlGame.Properties.Resources.PauseBtn;
            this.Pause.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Pause.FlatAppearance.BorderSize = 0;
            this.Pause.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Pause.Location = new System.Drawing.Point(725, 241);
            this.Pause.Name = "Pause";
            this.Pause.Size = new System.Drawing.Size(70, 47);
            this.Pause.TabIndex = 10;
            this.Pause.UseVisualStyleBackColor = false;
            this.Pause.Click += new System.EventHandler(this.Pause_Click);
            // 
            // Fullscreen
            // 
            this.Fullscreen.BackColor = System.Drawing.Color.Transparent;
            this.Fullscreen.BackgroundImage = global::FlightControlGame.Properties.Resources.FullBtn;
            this.Fullscreen.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Fullscreen.FlatAppearance.BorderSize = 0;
            this.Fullscreen.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Fullscreen.Location = new System.Drawing.Point(651, 241);
            this.Fullscreen.Name = "Fullscreen";
            this.Fullscreen.Size = new System.Drawing.Size(70, 47);
            this.Fullscreen.TabIndex = 11;
            this.Fullscreen.UseVisualStyleBackColor = false;
            this.Fullscreen.Click += new System.EventHandler(this.Fullscreen_Click);
            // 
            // MainMenu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::FlightControlGame.Properties.Resources.MainMenu69;
            this.ClientSize = new System.Drawing.Size(984, 531);
            this.ControlBox = false;
            this.Controls.Add(this.Fullscreen);
            this.Controls.Add(this.Pause);
            this.Controls.Add(this.Play);
            this.Controls.Add(this.HelpVideo);
            this.Controls.Add(this.QuitBtn);
            this.Controls.Add(this.NextBtn);
            this.Controls.Add(this.PlayBtn);
            this.Controls.Add(this.StatBtn);
            this.Controls.Add(this.OptionBtn);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.Name = "MainMenu";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Main Menu";
            this.Load += new System.EventHandler(this.MainMenu_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MainMenu_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.HelpVideo)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button OptionBtn;
        private System.Windows.Forms.Button StatBtn;
        private System.Windows.Forms.Button PlayBtn;
        private System.Windows.Forms.Button NextBtn;
        private System.Windows.Forms.Timer Movement;
        private System.Windows.Forms.Button QuitBtn;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private AxWMPLib.AxWindowsMediaPlayer HelpVideo;
        private System.Windows.Forms.Button Play;
        private System.Windows.Forms.Button Pause;
        private System.Windows.Forms.Button Fullscreen;
    }
}