﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing.Drawing2D;

namespace FlightControlGame
{
    public partial class FixerMap : Form
    {
        //Generate Random Number
        Random rnd1 = new Random();
        Random rnd2 = new Random();

        //Used as an index for active aircraft throughout the code
        public int activeAircraft = 0;

        //Used in creation of aircraft
        int PreviousDigit = -1;

        //X & Y of mouse click
        public int downX;
        public int downY;

        //Current speed of aircraft
        public double speed = 1;

        //Score
        public static int LandedAircraft = 0;

        //Used as an index in reassign ID
        public int i = -1;

        //Defines the sound player for each audio element
        System.Media.SoundPlayer Silent = new System.Media.SoundPlayer(Application.StartupPath + "\\Music\\Silent.wav");
        System.Media.SoundPlayer CrashSound = new System.Media.SoundPlayer(Application.StartupPath + "\\Music\\CrashNoise.wav");
        System.Media.SoundPlayer LandSound = new System.Media.SoundPlayer(Application.StartupPath + "\\Music\\LandNoise.wav");

        //Stores all of the aircraft objects inside one location
        List<Aircraft> Hangar = new List<Aircraft>();

        public FixerMap()
        {
            InitializeComponent();
        }

        //Runs when the map opens
        private void FixerMap_Load(object sender, EventArgs e)
        {
            //Resets score
            LandedAircraft = 0;
            
            //Creates one Aircraft to begin before timer creates others
            CreateAircraft();
            
            //Prevents screen flickering
            DoubleBuffered = true;

            //Plays silent audio to initialize soundplayer to reduce sound lag
            if (OptionMenu.MusicVol == false)
            {
                Silent.Play();
            }

            //Sets spawner time based on selected mode
            if (OptionMenu.Difficulty == "EASY")
            {
                SpawnAircraft.Interval = 7000;
            }
        }

        //Creates aircraft objects when called
        private void CreateAircraft()
        {
            //Only 4 planes made at any time
            if (i <= 4)
            {
                i++;
            }
            else
            {
                i = 0;
            }
            
            int SpawnDigit = rnd1.Next(0, 8);
            int TypeDigit = rnd2.Next(0, 8);
            
            if (PreviousDigit == SpawnDigit)
            {
                if (SpawnDigit == 0)
                {
                    SpawnDigit = 2;
                }
                else if (SpawnDigit < 9)
                {
                    SpawnDigit++;
                }
                else
                {
                    SpawnDigit = 1;
                }

            }

            //Object values
            int[] aircraftIDs = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13 };
            double[] aircraftXs = { 50, 150, 250, 350, 450, 550, 650, 750, 850};
            string[] aircraftTypes = { "Light", "Heavy", "Light", "Heli", "Heavy", "Light", "Heavy", "Light", "Heavy"};
            
            //Import object and apply values
            Aircraft aircraft = new Aircraft();
            aircraft.ID = aircraftIDs[i];
            aircraft.X = aircraftXs[SpawnDigit];
            aircraft.Y = 600;
            aircraft.Type = aircraftTypes[TypeDigit];

            //Set Previous Digit
            PreviousDigit = SpawnDigit;
            

            //Add image to each aircraft object
            aircraft.pic.Location = new Point((int)Math.Round(aircraft.X), (int)Math.Round(aircraft.Y));
            aircraft.pic.Name = ("AirImg" + aircraft.ID.ToString());

            if (aircraft.Type == "Heavy")
            {
                aircraft.pic.Size = new Size(34, 36);
                aircraft.pic.Image = Image.FromFile(Application.StartupPath + "\\Images\\Aircraft\\HeavyPlane.png");
            }
            else if (aircraft.Type == "Light")
            {
                aircraft.pic.Size = new Size(34, 36);
                aircraft.pic.Image = Image.FromFile(Application.StartupPath + "\\Images\\Aircraft\\Plane2.png");
            }
            else if (aircraft.Type == "Heli")
            {
                aircraft.pic.Size = new Size(34, 36);
                aircraft.pic.Image = Image.FromFile(Application.StartupPath + "\\Images\\Aircraft\\Heli.png");
            }

            aircraft.pic.BackColor = Color.Transparent;

            aircraft.pic.MouseDown += new MouseEventHandler(this.Aircraft_MouseDown);
            aircraft.pic.MouseUp += new MouseEventHandler(this.Aircraft_MouseUp);

            //Add controls and add to list
            Controls.Add(aircraft.pic);
            Hangar.Add(aircraft);

        }
        
        //Checks mouse X & Y of click location
        private void Aircraft_MouseDown(object sender, MouseEventArgs e)
        {
            PictureBox pic = sender as PictureBox;
            activeAircraft = Int32.Parse(pic.Name.Replace("AirImg", ""));
            downX = Cursor.Position.X;
            downY = Cursor.Position.Y;

        }

        //Checks mouse X & Y of release location then calls roate aircraft
        private void Aircraft_MouseUp(object sender, MouseEventArgs e)
        {
            double upX = Cursor.Position.X;
            double upY = Cursor.Position.Y;
            double disX = upX - downX;
            double disY = upY - downY;
            // tan(90) may not work fix later
            double angle = Math.Atan2(disY, disX);
            //convert to degrees
            angle = angle * (180 / Math.PI) + 90;

            //If No Collision Has Occured, Rotate Selected Aircraft
            if (speed > 0 && Hangar[activeAircraft].InsideMap == true)
            {
                rotateAircraft(angle);
                Hangar[activeAircraft].Rotation = angle;
            }
        }

        //Receives selected aircraft and rotates it
        private void rotateAircraft(double angle)
        {

            Bitmap rotatedImage = new Bitmap(Hangar[activeAircraft].pic.Image.Width, Hangar[activeAircraft].pic.Image.Height);
            rotatedImage.SetResolution(Hangar[activeAircraft].pic.Image.HorizontalResolution, Hangar[activeAircraft].pic.Image.VerticalResolution);
            using (Graphics g = Graphics.FromImage(rotatedImage))
            {
                g.TranslateTransform(rotatedImage.Width / 2, rotatedImage.Height / 2);
                g.RotateTransform((float)angle - (float)0);
                g.TranslateTransform(-rotatedImage.Width / 2, -rotatedImage.Height / 2);
                g.InterpolationMode = InterpolationMode.HighQualityBicubic;
                if (Hangar[activeAircraft].Type == "Heavy")
                {
                    g.DrawImage(Image.FromFile(Application.StartupPath + "\\Images\\Aircraft\\HeavyPlane.png"), new Point(0, 0));
                }
                else if (Hangar[activeAircraft].Type == "Light")
                {
                    g.DrawImage(Image.FromFile(Application.StartupPath + "\\Images\\Aircraft\\Plane2.png"), new Point(0, 0));
                }
                else if (Hangar[activeAircraft].Type == "Heli")
                {
                    g.DrawImage(Image.FromFile(Application.StartupPath + "\\Images\\Aircraft\\Heli.png"), new Point(0, 0));
                }

                g.Dispose();
                Hangar[activeAircraft].pic.Image = rotatedImage;
            }
        }

        //Gives the aircraft speed and sets it location, also checks if plane is within map
        private void MoveAircraft()
        {
            foreach (var aircraft in Hangar)
            {
                if (aircraft.Type == "Heavy")
                {
                    speed = 1;
                    aircraft.Y = aircraft.Y - speed * Math.Cos(aircraft.Rotation * Math.PI / 180);
                    aircraft.X = aircraft.X + speed * Math.Sin(aircraft.Rotation * Math.PI / 180);
                    aircraft.pic.Location = new Point((int)Math.Round(aircraft.X), (int)Math.Round(aircraft.Y));
                }
                else if (aircraft.Type == "Light")
                {
                    speed = 1.2;
                    aircraft.Y = aircraft.Y - speed * Math.Cos(aircraft.Rotation * Math.PI / 180);
                    aircraft.X = aircraft.X + speed * Math.Sin(aircraft.Rotation * Math.PI / 180);
                    aircraft.pic.Location = new Point((int)Math.Round(aircraft.X), (int)Math.Round(aircraft.Y));
                }
                else if (aircraft.Type == "Heli")
                {
                    speed = 1.1;
                    aircraft.Y = aircraft.Y - speed * Math.Cos(aircraft.Rotation * Math.PI / 180);
                    aircraft.X = aircraft.X + speed * Math.Sin(aircraft.Rotation * Math.PI / 180);
                    aircraft.pic.Location = new Point((int)Math.Round(aircraft.X), (int)Math.Round(aircraft.Y));
                }

                //Out of Bounds
                if (aircraft.Y >= 492 && aircraft.InsideMap == true || aircraft.X <= 0 && aircraft.InsideMap == true || aircraft.Y <= 0 && aircraft.InsideMap == true || aircraft.X >= 942 && aircraft.InsideMap == true)
                {
                    speed = 0;
                    aircraft.pic.BackColor = Color.Red;

                    if (OptionMenu.CrashVol == true)
                    {
                        CrashSound.Play();
                    }
                    
                    Crash.Stop();
                    SpawnAircraft.Stop();
                    MoveObjects.Stop();

                    Popup PU = new Popup();
                    PU.ShowDialog(this);
                    goto LoopEnd;
                }

                //Enter Map
                if (aircraft.Y < 492 && aircraft.Y > 0 && aircraft.X > 0 && aircraft.X < 942)
                {
                    aircraft.InsideMap = true;
                }
            }
            LoopEnd:;
        }

        //Always Checks for Collisions
        private void CheckCrash()
        {
            foreach (var aircraft in Hangar)
            {
                foreach (var aircraft2 in Hangar)
                {
                    if (aircraft != aircraft2)
                    {
                        if (aircraft.pic.Bounds.IntersectsWith(aircraft2.pic.Bounds))
                        {
                            if (OptionMenu.CrashVol == true)
                            {
                                CrashSound.Play();
                            }

                            aircraft.pic.BackColor = Color.Red;
                            aircraft2.pic.BackColor = Color.Red;
                            speed = 0;

                            Crash.Stop();
                            SpawnAircraft.Stop();
                            MoveObjects.Stop();

                            Popup PU = new Popup();
                            PU.ShowDialog(this);
                            goto LoopEnd;
                        }
                    }
                }
            }
            LoopEnd:;

        }

        //Always Checks for Landings
        private void CheckLanding()
        {
            foreach (var aircraft in Hangar)
            {
                //Red Runway
                if (aircraft.pic.Bounds.IntersectsWith(RedLanding.Bounds))
                {
                    if (aircraft.Rotation >= 75 && aircraft.Rotation <= 105 && aircraft.Type == "Heavy")
                    {
                        LandedAircraft++;

                        if (OptionMenu.LandVol == true)
                        {
                            LandSound.Play();
                        }

                        aircraft.pic.Dispose();
                        Hangar.Remove(aircraft);
                        ReassignID();
                        break;
                    }
                }

                //Yellow Runway
                if (aircraft.pic.Bounds.IntersectsWith(YellowLanding.Bounds))
                {
                    if (aircraft.Rotation >= 25 && aircraft.Rotation <= 65 && aircraft.Type == "Light")
                    {
                        LandedAircraft++;

                        if (OptionMenu.LandVol == true)
                        {
                            LandSound.Play();
                        }

                        aircraft.pic.Dispose();
                        Hangar.Remove(aircraft);
                        ReassignID();
                        break;
                    }
                }

                //Pad
                if (aircraft.pic.Bounds.IntersectsWith(HPad.Bounds))
                {
                    if (aircraft.Type == "Heli")
                    {
                        LandedAircraft++;

                        if (OptionMenu.LandVol == true)
                        {
                            LandSound.Play();
                        }

                        aircraft.pic.Dispose();
                        Hangar.Remove(aircraft);
                        ReassignID();
                        break;
                    }
                }
            }
        }

        //Reassigns aircraft IDs after plane lands
        private void ReassignID()
        {
            foreach (var aircraft in Hangar)
            {
                aircraft.ID = Hangar.IndexOf(aircraft);
                aircraft.pic.Name = ("AirImg" + aircraft.ID.ToString());
                i = Hangar.Count() + 1;
            }
        }
        
        //Calls crash & landing functions
        private void Crash_Tick_1(object sender, EventArgs e)
        {
            CheckCrash();
            CheckLanding();
        }

        //Spawns aircraft based on given interval
        private void SpawnAircraft_Tick_1(object sender, EventArgs e)
        {
            if (Hangar.Count() < 5)
            {
                CreateAircraft();

                foreach (var aircraft in Hangar)
                {
                    aircraft.ID = Hangar.IndexOf(aircraft);
                    aircraft.pic.Name = ("AirImg" + aircraft.ID.ToString());
                }
            }
        }

        //Calls move aircraft and sets score
        private void MoveObjects_Tick_1(object sender, EventArgs e)
        {
            MoveAircraft();
            //Checks Score
            LandedTxt.Text = ("Landed: " + LandedAircraft);
        }

        //Go to menu
        private void MenuThing_Click(object sender, EventArgs e)
        {
            Hide();
            MainMenu MM = new MainMenu();
            MM.ShowDialog();
        }
    }
}
