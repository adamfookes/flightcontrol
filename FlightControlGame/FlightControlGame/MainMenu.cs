﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FlightControlGame
{
    public partial class MainMenu : Form
    {
        public int CurrentMap = 1;
        public static bool Playing = false;
        public bool FullscreenMode = false;
        
        public MainMenu()
        {
            InitializeComponent();
        }

        private void MainMenu_Load(object sender, EventArgs e)
        {
            System.Media.SoundPlayer Music = new System.Media.SoundPlayer(Application.StartupPath + "\\Music\\Kick.wav");

            if (OptionMenu.MusicVol == true && Playing == false)
            {
                Music.PlayLooping();
                Playing = true;
            }
            else if (OptionMenu.MusicVol == false)
            {
                Music.Stop();
                Playing = false;
            }
        }

        private void MainMenu_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                if (FullscreenMode == true)
                {
                    //MoveScreen
                    HelpVideo.Location = new Point(578, 102);
                    HelpVideo.Size = new Size(222, 133);
                    FullscreenMode = false;

                    //MoveButtons
                    Play.Location = new Point(578, 241);
                    Play.BackColor = Color.Transparent;
                    Pause.Location = new Point(725, 241);
                    Pause.BackColor = Color.Transparent;
                    Fullscreen.Visible = true;
                }
            }
        }

        private void OptionBtn_Click(object sender, EventArgs e)
        {
            Movement.Stop();
            this.Hide();
            OptionMenu OM = new OptionMenu();
            OM.ShowDialog();
        }

        private void StatBtn_Click(object sender, EventArgs e)
        {
            Movement.Stop();
            this.Hide();
            StatMenu SM = new StatMenu();
            SM.ShowDialog();
        }

        private void PlayBtn_Click(object sender, EventArgs e)
        {
            if (CurrentMap == 0 || CurrentMap == 1)
            {
                Popup.Map = 1;
                Movement.Stop();

                this.Hide();
                FixerMap mp1 = new FixerMap();
                mp1.ShowDialog();
            }
            else if (CurrentMap == 2)
            {
                Popup.Map = 2;
                Movement.Stop();

                this.Hide();
                Map2 mp2 = new Map2();
                mp2.ShowDialog();
            }
            
        }

        private void NextBtn_Click(object sender, EventArgs e)
        {
            if (CurrentMap < 2)
            {
                CurrentMap++;
            }
            else
            {
                CurrentMap = 1;
            }
        }

        private void Movement_Tick(object sender, EventArgs e)
        {
            if (CurrentMap == 1)
            {
                this.BackgroundImage = Image.FromFile(Application.StartupPath + "\\Images\\Map\\MainMenu69.jpg");
            }
            else if (CurrentMap == 2)
            {
                this.BackgroundImage = Image.FromFile(Application.StartupPath + "\\Images\\Map\\MainMenu96.jpg");
            }
        }

        private void QuitBtn_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void Play_Click(object sender, EventArgs e)
        {
            WMPLib.IWMPControls3 controls = (WMPLib.IWMPControls3)HelpVideo.Ctlcontrols;
            controls.play();
        }

        private void Fullscreen_Click(object sender, EventArgs e)
        {
            if (FullscreenMode == false)
            {
                //MoveScreen
                HelpVideo.Location = new Point(0, 0);
                HelpVideo.Size = new Size(1000, 530);

                //MoveButtons
                Play.Location = new Point(826, 472);
                Play.BackColor = Color.White;
                Pause.Location = new Point(902, 472);
                Pause.BackColor = Color.White;
                Fullscreen.Visible = false;

                FullscreenMode = true;
            }
        }

        private void Pause_Click(object sender, EventArgs e)
        {
            WMPLib.IWMPControls3 controls = (WMPLib.IWMPControls3)HelpVideo.Ctlcontrols;
            controls.pause();
        }
    }
}
