﻿namespace FlightControlGame
{
    partial class FixerMap
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FixerMap));
            this.MoveObjects = new System.Windows.Forms.Timer(this.components);
            this.Crash = new System.Windows.Forms.Timer(this.components);
            this.SpawnAircraft = new System.Windows.Forms.Timer(this.components);
            this.LandedTxt = new System.Windows.Forms.Label();
            this.RedLanding = new System.Windows.Forms.PictureBox();
            this.YellowLanding = new System.Windows.Forms.PictureBox();
            this.MenuThing = new System.Windows.Forms.Button();
            this.HPad = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.RedLanding)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.YellowLanding)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.HPad)).BeginInit();
            this.SuspendLayout();
            // 
            // MoveObjects
            // 
            this.MoveObjects.Enabled = true;
            this.MoveObjects.Tick += new System.EventHandler(this.MoveObjects_Tick_1);
            // 
            // Crash
            // 
            this.Crash.Enabled = true;
            this.Crash.Interval = 1;
            this.Crash.Tick += new System.EventHandler(this.Crash_Tick_1);
            // 
            // SpawnAircraft
            // 
            this.SpawnAircraft.Enabled = true;
            this.SpawnAircraft.Interval = 5000;
            this.SpawnAircraft.Tick += new System.EventHandler(this.SpawnAircraft_Tick_1);
            // 
            // LandedTxt
            // 
            this.LandedTxt.AutoSize = true;
            this.LandedTxt.BackColor = System.Drawing.Color.Transparent;
            this.LandedTxt.Font = new System.Drawing.Font("Verdana", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LandedTxt.ForeColor = System.Drawing.Color.White;
            this.LandedTxt.Location = new System.Drawing.Point(12, 9);
            this.LandedTxt.Name = "LandedTxt";
            this.LandedTxt.Size = new System.Drawing.Size(117, 23);
            this.LandedTxt.TabIndex = 3;
            this.LandedTxt.Text = "Landed: 0";
            // 
            // RedLanding
            // 
            this.RedLanding.BackColor = System.Drawing.Color.Transparent;
            this.RedLanding.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.RedLanding.Location = new System.Drawing.Point(207, 229);
            this.RedLanding.Name = "RedLanding";
            this.RedLanding.Size = new System.Drawing.Size(31, 15);
            this.RedLanding.TabIndex = 4;
            this.RedLanding.TabStop = false;
            this.RedLanding.Visible = false;
            // 
            // YellowLanding
            // 
            this.YellowLanding.BackColor = System.Drawing.Color.Transparent;
            this.YellowLanding.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.YellowLanding.Location = new System.Drawing.Point(593, 363);
            this.YellowLanding.Name = "YellowLanding";
            this.YellowLanding.Size = new System.Drawing.Size(16, 16);
            this.YellowLanding.TabIndex = 5;
            this.YellowLanding.TabStop = false;
            this.YellowLanding.Visible = false;
            // 
            // MenuThing
            // 
            this.MenuThing.BackColor = System.Drawing.Color.Transparent;
            this.MenuThing.BackgroundImage = global::FlightControlGame.Properties.Resources.MenuBtn;
            this.MenuThing.Cursor = System.Windows.Forms.Cursors.Hand;
            this.MenuThing.FlatAppearance.BorderSize = 0;
            this.MenuThing.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.MenuThing.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.MenuThing.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.MenuThing.Location = new System.Drawing.Point(902, 12);
            this.MenuThing.Name = "MenuThing";
            this.MenuThing.Size = new System.Drawing.Size(70, 45);
            this.MenuThing.TabIndex = 6;
            this.MenuThing.TabStop = false;
            this.MenuThing.UseVisualStyleBackColor = false;
            this.MenuThing.Click += new System.EventHandler(this.MenuThing_Click);
            // 
            // HPad
            // 
            this.HPad.BackColor = System.Drawing.Color.Transparent;
            this.HPad.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.HPad.Location = new System.Drawing.Point(471, 153);
            this.HPad.Name = "HPad";
            this.HPad.Size = new System.Drawing.Size(32, 34);
            this.HPad.TabIndex = 9;
            this.HPad.TabStop = false;
            this.HPad.Visible = false;
            // 
            // FixerMap
            // 
            this.AccessibleName = "";
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Maroon;
            this.BackgroundImage = global::FlightControlGame.Properties.Resources.Map13;
            this.ClientSize = new System.Drawing.Size(984, 531);
            this.ControlBox = false;
            this.Controls.Add(this.HPad);
            this.Controls.Add(this.MenuThing);
            this.Controls.Add(this.YellowLanding);
            this.Controls.Add(this.RedLanding);
            this.Controls.Add(this.LandedTxt);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FixerMap";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Map 1";
            this.TransparencyKey = System.Drawing.Color.Maroon;
            this.Load += new System.EventHandler(this.FixerMap_Load);
            ((System.ComponentModel.ISupportInitialize)(this.RedLanding)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.YellowLanding)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.HPad)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Timer MoveObjects;
        private System.Windows.Forms.Timer Crash;
        private System.Windows.Forms.Timer SpawnAircraft;
        private System.Windows.Forms.Label LandedTxt;
        private System.Windows.Forms.PictureBox RedLanding;
        private System.Windows.Forms.PictureBox YellowLanding;
        private System.Windows.Forms.Button MenuThing;
        private System.Windows.Forms.PictureBox HPad;
    }
}