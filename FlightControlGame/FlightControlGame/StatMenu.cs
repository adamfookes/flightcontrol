﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text.RegularExpressions;

namespace FlightControlGame
{
    public partial class StatMenu : Form
    {
        StringBuilder sb = new StringBuilder();

        public int ScoreComparision = 0;
        public bool Swapped1 = true;
        public bool Swapped2 = true;

        //Map1
        public string[] lines = System.IO.File.ReadAllLines(Application.StartupPath + "\\Saves\\Map1Save.txt");
        //Map2
        public string[] bines = System.IO.File.ReadAllLines(Application.StartupPath + "\\Saves\\Map2Save.txt");

        List<ScoreObj1> scoresList1 = new List<ScoreObj1>();
        List<ScoreObj2> scoresList2 = new List<ScoreObj2>();
        List<string> displayList1 = new List<string>();
        List<string> displayList2 = new List<string>();

        public int CurrentMap = 2;

        public StatMenu()
        {
            InitializeComponent();
        }

        private void MainBtn_Click(object sender, EventArgs e)
        {
            this.Hide();
            MainMenu MM = new MainMenu();
            MM.ShowDialog();
        }

        private void OptionBtn_Click(object sender, EventArgs e)
        {
            this.Hide();
            OptionMenu OM = new OptionMenu();
            OM.ShowDialog();
        }

        private void StatMenu_Load(object sender, EventArgs e)
        {
            
            SortScores1();
            SortScores2();
            

            sb.AppendLine("Map 1 Top Scores:");
            foreach (var line in displayList1)
            {
                sb.AppendLine(line);
            }

            TopScoresText.Text = sb.ToString().Replace("\n", Environment.NewLine);
        }
        
        private void SortScores1()
        {
            displayList1.Clear();
            //Map1
            int index = 0;

            while (index < lines.Length)
            {
                int i = lines[index].IndexOf(" ") + 1;
                int str = Int32.Parse(lines[index].Substring(i));
                string name = Regex.Replace(lines[index], @"[\d-]", "");

                ScoreObj1 scoreThing = new ScoreObj1();
                scoreThing.score = str;
                scoreThing.scoreID = name;
                scoresList1.Add(scoreThing);

                index++;
            }
            
            Swapped1 = true;
            while (Swapped1 == true)
            {
                Swapped1 = false;
                ScoreComparision = 0;

                while (ScoreComparision < scoresList1.Count - 1)
                {
                    if (scoresList1[ScoreComparision].score < scoresList1[ScoreComparision + 1].score)
                    {
                        int tempScore;
                        string tempName;

                        tempScore = scoresList1[ScoreComparision].score;
                        tempName = scoresList1[ScoreComparision].scoreID;

                        scoresList1[ScoreComparision].score = scoresList1[ScoreComparision + 1].score;
                        scoresList1[ScoreComparision].scoreID = scoresList1[ScoreComparision + 1].scoreID;

                        scoresList1[ScoreComparision + 1].score = tempScore;
                        scoresList1[ScoreComparision + 1].scoreID = tempName;
                        Swapped1 = true;
                    }
                    ScoreComparision++;
                }
            }

            addNames1();
        }

        private void SortScores2()
        {
            displayList2.Clear();
            //Map 2
            int index = 0;

            while (index < bines.Length)
            {
                int i = bines[index].IndexOf(" ") + 1;
                int str = Int32.Parse(bines[index].Substring(i));
                string name = Regex.Replace(bines[index], @"[\d-]", "");

                ScoreObj2 scoreThing2 = new ScoreObj2();
                scoreThing2.score = str;
                scoreThing2.scoreID = name;
                scoresList2.Add(scoreThing2);

                index++;
            }

            Console.WriteLine("Prior");
            foreach (var item in scoresList2)
            {
                Console.WriteLine(item.scoreID + item.score);
            }

            Swapped2 = true;
            while (Swapped2 == true)
            {
                Swapped2 = false;
                ScoreComparision = 0;

                while (ScoreComparision < scoresList2.Count - 1)
                {
                    if (scoresList2[ScoreComparision].score < scoresList2[ScoreComparision + 1].score)
                    {
                        int tempScore;
                        string tempName;

                        tempScore = scoresList2[ScoreComparision].score;
                        tempName = scoresList2[ScoreComparision].scoreID;

                        scoresList2[ScoreComparision].score = scoresList2[ScoreComparision + 1].score;
                        scoresList2[ScoreComparision].scoreID = scoresList2[ScoreComparision + 1].scoreID;

                        scoresList2[ScoreComparision + 1].score = tempScore;
                        scoresList2[ScoreComparision + 1].scoreID = tempName;
                        Swapped2 = true;
                    }
                    ScoreComparision++;
                }
                
            }
            addNames2();
        }

        private void addNames1()
        {
            int index = 0;
            while (index < 4)
            {
                string nameAdd = scoresList1[index].scoreID;
                int scoreAdd = scoresList1[index].score;
                displayList1.Add(nameAdd + " " + scoreAdd.ToString());

                index++;
            }
        }

        private void addNames2()
        {
            int index = 0;
            while (index < 4)
            {
                string nameAdd = scoresList2[index].scoreID;
                int scoreAdd = scoresList2[index].score;
                displayList2.Add(nameAdd + " " + scoreAdd.ToString());

                index++;
            }
        }

        private void NextBtn_Click(object sender, EventArgs e)
        {
            sb.Clear();
            
            if (CurrentMap == 1)
            {
                sb.AppendLine("Map 1 Top Scores:");
                foreach (var line in displayList1)
                {
                    sb.AppendLine(line);
                    
                }
                this.BackgroundImage = Image.FromFile(Application.StartupPath + "\\Images\\Map\\StatMenu1.jpg");
                CurrentMap = 2;
            }
            else if(CurrentMap == 2)
            {
                sb.AppendLine("Map 2 Top Scores:");
                foreach (var bine in displayList2)
                {    
                    sb.AppendLine(bine);
                }
                this.BackgroundImage = Image.FromFile(Application.StartupPath + "\\Images\\Map\\StatMenu2.jpg");
                CurrentMap = 1;
            }

            TopScoresText.Text = sb.ToString().Replace("\n", Environment.NewLine);
        }
        
        private void TopScoresText_Click(object sender, EventArgs e)
        {

        }
    }
}
