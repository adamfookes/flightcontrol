﻿namespace FlightControlGame
{
    partial class Popup
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Popup));
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.ChangeScreen = new System.Windows.Forms.Button();
            this.Save = new System.Windows.Forms.Button();
            this.SaveTxt = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // textBox1
            // 
            this.textBox1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(233)))), ((int)(((byte)(225)))), ((int)(((byte)(222)))));
            this.textBox1.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox1.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.textBox1.Location = new System.Drawing.Point(115, 85);
            this.textBox1.MaxLength = 15;
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(270, 31);
            this.textBox1.TabIndex = 0;
            // 
            // ChangeScreen
            // 
            this.ChangeScreen.BackColor = System.Drawing.Color.Transparent;
            this.ChangeScreen.BackgroundImage = global::FlightControlGame.Properties.Resources.MenuBtn;
            this.ChangeScreen.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ChangeScreen.FlatAppearance.BorderSize = 0;
            this.ChangeScreen.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.ChangeScreen.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.ChangeScreen.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ChangeScreen.Location = new System.Drawing.Point(408, 130);
            this.ChangeScreen.Name = "ChangeScreen";
            this.ChangeScreen.Size = new System.Drawing.Size(70, 45);
            this.ChangeScreen.TabIndex = 1;
            this.ChangeScreen.TabStop = false;
            this.ChangeScreen.UseVisualStyleBackColor = false;
            this.ChangeScreen.Click += new System.EventHandler(this.ChangeScreen_Click);
            // 
            // Save
            // 
            this.Save.BackColor = System.Drawing.Color.Transparent;
            this.Save.BackgroundImage = global::FlightControlGame.Properties.Resources.SaveBtn;
            this.Save.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Save.FlatAppearance.BorderSize = 0;
            this.Save.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.Save.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.Save.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Save.Location = new System.Drawing.Point(21, 130);
            this.Save.Name = "Save";
            this.Save.Size = new System.Drawing.Size(70, 45);
            this.Save.TabIndex = 2;
            this.Save.TabStop = false;
            this.Save.UseVisualStyleBackColor = false;
            this.Save.Click += new System.EventHandler(this.Save_Click);
            // 
            // SaveTxt
            // 
            this.SaveTxt.Font = new System.Drawing.Font("Verdana", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SaveTxt.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.SaveTxt.Location = new System.Drawing.Point(155, 155);
            this.SaveTxt.Name = "SaveTxt";
            this.SaveTxt.Size = new System.Drawing.Size(190, 25);
            this.SaveTxt.TabIndex = 3;
            this.SaveTxt.Text = "SAVED";
            this.SaveTxt.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Popup
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::FlightControlGame.Properties.Resources.PopupDesign1;
            this.ClientSize = new System.Drawing.Size(499, 196);
            this.ControlBox = false;
            this.Controls.Add(this.SaveTxt);
            this.Controls.Add(this.Save);
            this.Controls.Add(this.ChangeScreen);
            this.Controls.Add(this.textBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Popup";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Game Over";
            this.Load += new System.EventHandler(this.Popup_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button ChangeScreen;
        private System.Windows.Forms.Button Save;
        private System.Windows.Forms.Label SaveTxt;
    }
}