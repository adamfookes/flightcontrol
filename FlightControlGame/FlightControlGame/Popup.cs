﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Text.RegularExpressions;

namespace FlightControlGame
{
    public partial class Popup : Form
    {
        public static string PlayerName = "JP1JP2JP1";
        public static int Map = 1;
        public bool TakenName = false;
        public bool CanSave = true;

        public Popup()
        {
            InitializeComponent();
        }

        private void ChangeScreen_Click(object sender, EventArgs e)
        {
            this.Hide();

            Owner.Hide();
            
            MainMenu MM = new MainMenu();
            MM.ShowDialog();
        }

        private void Save_Click(object sender, EventArgs e)
        {
            if (textBox1.Text != "")
            {
                PlayerName = textBox1.Text;
                if (CanSave == true)
                {
                    TakenName = false;
                    SaveStats();
                }
                
            }
        }

        private void SaveStats()
        {
            CanSave = false;

            if (Map == 1)
            {
                string[] lines = System.IO.File.ReadAllLines(Application.StartupPath + "\\Saves\\Map1Save.txt");

                foreach (var line in lines)
                {
                    if (line.Contains(PlayerName))
                    {
                        TakenName = true;
                        break;
                    }
                    else if(line.Contains(PlayerName) == false)
                    {
                        TakenName = false;
                    }
                }

                if (TakenName == false)
                {
                    if (lines.Contains(PlayerName) == false)
                    {
                        string DataToSave = (PlayerName + " " + FixerMap.LandedAircraft);
                        File.AppendAllText(Path.Combine((Application.StartupPath), "Saves\\Map1Save.txt"), DataToSave + Environment.NewLine);
                        SaveTxt.Text = "SAVED";
                    }
                }
                else if (TakenName == true)
                {
                    CanSave = true;
                    MessageBox.Show("Name Taken");
                }
            }
            else if (Map == 2)
            {
                string[] lines = System.IO.File.ReadAllLines(Application.StartupPath + "\\Saves\\Map2Save.txt");

                foreach (var line in lines)
                {
                    if (line.Contains(PlayerName))
                    {

                        TakenName = true;
                        break;

                    }
                    else if (line.Contains(PlayerName) == false)
                    {
                        TakenName = false;
                    }
                }

                if (TakenName == false)
                {
                    if (lines.Contains(PlayerName) == false)
                    {
                        string DataToSave = (PlayerName + " " + Map2.LandedAircraft);
                        File.AppendAllText(Path.Combine((Application.StartupPath), "Saves\\Map2Save.txt"), DataToSave + Environment.NewLine);
                        SaveTxt.Text = "SAVED";
                    }
                }
                else if (TakenName == true)
                {
                    CanSave = true;
                    MessageBox.Show("Name Taken");
                }
            }
            
            
        }

        private void Popup_Load(object sender, EventArgs e)
        {
            Owner.ShowInTaskbar = false;
            SaveTxt.Text = "";
        }
    }
}
