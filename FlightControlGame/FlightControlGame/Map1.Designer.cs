﻿namespace FlightControlGame
{
    partial class Map1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.MoveStuff = new System.Windows.Forms.Timer(this.components);
            this.MenuThing = new System.Windows.Forms.Button();
            this.CrashStuff = new System.Windows.Forms.Timer(this.components);
            this.RedLanding = new System.Windows.Forms.PictureBox();
            this.LandedTxt = new System.Windows.Forms.Label();
            this.YellowLanding = new System.Windows.Forms.PictureBox();
            this.SpawnAircraftStuff = new System.Windows.Forms.Timer(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.RedLanding)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.YellowLanding)).BeginInit();
            this.SuspendLayout();
            // 
            // MoveStuff
            // 
            this.MoveStuff.Enabled = true;
            this.MoveStuff.Tick += new System.EventHandler(this.MoveObjects_Tick);
            // 
            // MenuThing
            // 
            this.MenuThing.BackColor = System.Drawing.Color.Transparent;
            this.MenuThing.BackgroundImage = global::FlightControlGame.Properties.Resources.MenuBtn;
            this.MenuThing.Cursor = System.Windows.Forms.Cursors.Hand;
            this.MenuThing.FlatAppearance.BorderSize = 0;
            this.MenuThing.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.MenuThing.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.MenuThing.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.MenuThing.Location = new System.Drawing.Point(902, 12);
            this.MenuThing.Name = "MenuThing";
            this.MenuThing.Size = new System.Drawing.Size(70, 45);
            this.MenuThing.TabIndex = 0;
            this.MenuThing.TabStop = false;
            this.MenuThing.UseVisualStyleBackColor = false;
            this.MenuThing.Click += new System.EventHandler(this.MenuThing_Click);
            // 
            // CrashStuff
            // 
            this.CrashStuff.Enabled = true;
            this.CrashStuff.Interval = 1;
            this.CrashStuff.Tick += new System.EventHandler(this.Crash_Tick);
            // 
            // RedLanding
            // 
            this.RedLanding.BackColor = System.Drawing.Color.Transparent;
            this.RedLanding.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.RedLanding.Location = new System.Drawing.Point(207, 229);
            this.RedLanding.Name = "RedLanding";
            this.RedLanding.Size = new System.Drawing.Size(31, 15);
            this.RedLanding.TabIndex = 1;
            this.RedLanding.TabStop = false;
            this.RedLanding.Visible = false;
            // 
            // LandedTxt
            // 
            this.LandedTxt.AutoSize = true;
            this.LandedTxt.BackColor = System.Drawing.Color.Transparent;
            this.LandedTxt.Font = new System.Drawing.Font("Verdana", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LandedTxt.Location = new System.Drawing.Point(12, 9);
            this.LandedTxt.Name = "LandedTxt";
            this.LandedTxt.Size = new System.Drawing.Size(117, 23);
            this.LandedTxt.TabIndex = 2;
            this.LandedTxt.Text = "Landed: 0";
            // 
            // YellowLanding
            // 
            this.YellowLanding.BackColor = System.Drawing.Color.Transparent;
            this.YellowLanding.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.YellowLanding.Location = new System.Drawing.Point(591, 364);
            this.YellowLanding.Name = "YellowLanding";
            this.YellowLanding.Size = new System.Drawing.Size(16, 16);
            this.YellowLanding.TabIndex = 3;
            this.YellowLanding.TabStop = false;
            this.YellowLanding.Visible = false;
            // 
            // SpawnAircraftStuff
            // 
            this.SpawnAircraftStuff.Enabled = true;
            this.SpawnAircraftStuff.Interval = 5000;
            this.SpawnAircraftStuff.Tick += new System.EventHandler(this.SpawnAircraft_Tick);
            // 
            // Map1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Black;
            this.BackgroundImage = global::FlightControlGame.Properties.Resources.Map12;
            this.ClientSize = new System.Drawing.Size(984, 531);
            this.Controls.Add(this.YellowLanding);
            this.Controls.Add(this.LandedTxt);
            this.Controls.Add(this.MenuThing);
            this.Controls.Add(this.RedLanding);
            this.ForeColor = System.Drawing.Color.Transparent;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "Map1";
            this.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Map 1";
            this.TransparencyKey = System.Drawing.Color.Black;
            this.Load += new System.EventHandler(this.Map1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.RedLanding)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.YellowLanding)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Timer MoveStuff;
        private System.Windows.Forms.Button MenuThing;
        private System.Windows.Forms.Timer CrashStuff;
        private System.Windows.Forms.PictureBox RedLanding;
        private System.Windows.Forms.Label LandedTxt;
        private System.Windows.Forms.PictureBox YellowLanding;
        private System.Windows.Forms.Timer SpawnAircraftStuff;
    }
}