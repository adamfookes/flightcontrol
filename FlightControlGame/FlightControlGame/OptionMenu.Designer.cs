﻿namespace FlightControlGame
{
    partial class OptionMenu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(OptionMenu));
            this.MainBtn = new System.Windows.Forms.Button();
            this.StatBtn = new System.Windows.Forms.Button();
            this.checkedListBox1 = new System.Windows.Forms.CheckedListBox();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // MainBtn
            // 
            this.MainBtn.BackColor = System.Drawing.Color.Transparent;
            this.MainBtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.MainBtn.FlatAppearance.BorderSize = 0;
            this.MainBtn.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.MainBtn.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.MainBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.MainBtn.ForeColor = System.Drawing.Color.Transparent;
            this.MainBtn.Location = new System.Drawing.Point(851, 23);
            this.MainBtn.Name = "MainBtn";
            this.MainBtn.Size = new System.Drawing.Size(63, 95);
            this.MainBtn.TabIndex = 2;
            this.MainBtn.TabStop = false;
            this.MainBtn.UseVisualStyleBackColor = false;
            this.MainBtn.Click += new System.EventHandler(this.MainBtn_Click);
            // 
            // StatBtn
            // 
            this.StatBtn.BackColor = System.Drawing.Color.Transparent;
            this.StatBtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.StatBtn.FlatAppearance.BorderSize = 0;
            this.StatBtn.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.StatBtn.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.StatBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.StatBtn.ForeColor = System.Drawing.Color.Transparent;
            this.StatBtn.Location = new System.Drawing.Point(851, 234);
            this.StatBtn.Name = "StatBtn";
            this.StatBtn.Size = new System.Drawing.Size(63, 95);
            this.StatBtn.TabIndex = 3;
            this.StatBtn.TabStop = false;
            this.StatBtn.UseVisualStyleBackColor = false;
            this.StatBtn.Click += new System.EventHandler(this.StatBtn_Click);
            // 
            // checkedListBox1
            // 
            this.checkedListBox1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(233)))), ((int)(((byte)(225)))), ((int)(((byte)(222)))));
            this.checkedListBox1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.checkedListBox1.CheckOnClick = true;
            this.checkedListBox1.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkedListBox1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(228)))), ((int)(((byte)(76)))), ((int)(((byte)(66)))));
            this.checkedListBox1.FormattingEnabled = true;
            this.checkedListBox1.Items.AddRange(new object[] {
            "Music",
            "Crash Effects",
            "Landing Effects"});
            this.checkedListBox1.Location = new System.Drawing.Point(564, 252);
            this.checkedListBox1.Name = "checkedListBox1";
            this.checkedListBox1.Size = new System.Drawing.Size(248, 104);
            this.checkedListBox1.TabIndex = 4;
            this.checkedListBox1.TabStop = false;
            this.checkedListBox1.SelectedIndexChanged += new System.EventHandler(this.checkedListBox1_SelectedIndexChanged);
            // 
            // comboBox1
            // 
            this.comboBox1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(233)))), ((int)(((byte)(225)))), ((int)(((byte)(222)))));
            this.comboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox1.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBox1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(228)))), ((int)(((byte)(76)))), ((int)(((byte)(66)))));
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Items.AddRange(new object[] {
            "EASY",
            "INTERMEDIATE",
            "HARD"});
            this.comboBox1.Location = new System.Drawing.Point(197, 252);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(230, 31);
            this.comboBox1.TabIndex = 5;
            this.comboBox1.TabStop = false;
            this.comboBox1.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // OptionMenu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::FlightControlGame.Properties.Resources.OptionMenu2;
            this.ClientSize = new System.Drawing.Size(984, 531);
            this.ControlBox = false;
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.checkedListBox1);
            this.Controls.Add(this.StatBtn);
            this.Controls.Add(this.MainBtn);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "OptionMenu";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Option Menu";
            this.Load += new System.EventHandler(this.OptionMenu_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button MainBtn;
        private System.Windows.Forms.Button StatBtn;
        private System.Windows.Forms.CheckedListBox checkedListBox1;
        private System.Windows.Forms.ComboBox comboBox1;
    }
}