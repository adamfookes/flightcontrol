﻿namespace FlightControlGame
{
    partial class Map2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Map2));
            this.Menu = new System.Windows.Forms.Button();
            this.MoveObjects = new System.Windows.Forms.Timer(this.components);
            this.SpawnAircraft = new System.Windows.Forms.Timer(this.components);
            this.Crash = new System.Windows.Forms.Timer(this.components);
            this.LandedTxt = new System.Windows.Forms.Label();
            this.RedLanding = new System.Windows.Forms.PictureBox();
            this.YellowLanding = new System.Windows.Forms.PictureBox();
            this.WhiteLanding = new System.Windows.Forms.PictureBox();
            this.HPad = new System.Windows.Forms.PictureBox();
            this.HPad2 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.RedLanding)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.YellowLanding)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.WhiteLanding)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.HPad)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.HPad2)).BeginInit();
            this.SuspendLayout();
            // 
            // Menu
            // 
            this.Menu.BackColor = System.Drawing.Color.Transparent;
            this.Menu.BackgroundImage = global::FlightControlGame.Properties.Resources.MenuBtn;
            this.Menu.FlatAppearance.BorderSize = 0;
            this.Menu.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.Menu.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.Menu.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Menu.Location = new System.Drawing.Point(902, 12);
            this.Menu.Name = "Menu";
            this.Menu.Size = new System.Drawing.Size(70, 45);
            this.Menu.TabIndex = 1;
            this.Menu.TabStop = false;
            this.Menu.UseVisualStyleBackColor = false;
            this.Menu.Click += new System.EventHandler(this.Menu_Click);
            // 
            // MoveObjects
            // 
            this.MoveObjects.Enabled = true;
            this.MoveObjects.Tick += new System.EventHandler(this.MoveObjects_Tick);
            // 
            // SpawnAircraft
            // 
            this.SpawnAircraft.Enabled = true;
            this.SpawnAircraft.Interval = 5000;
            this.SpawnAircraft.Tick += new System.EventHandler(this.SpawnAircraft_Tick);
            // 
            // Crash
            // 
            this.Crash.Enabled = true;
            this.Crash.Interval = 1;
            this.Crash.Tick += new System.EventHandler(this.Crash_Tick);
            // 
            // LandedTxt
            // 
            this.LandedTxt.AutoSize = true;
            this.LandedTxt.BackColor = System.Drawing.Color.Transparent;
            this.LandedTxt.Font = new System.Drawing.Font("Verdana", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LandedTxt.ForeColor = System.Drawing.Color.White;
            this.LandedTxt.Location = new System.Drawing.Point(12, 9);
            this.LandedTxt.Name = "LandedTxt";
            this.LandedTxt.Size = new System.Drawing.Size(117, 23);
            this.LandedTxt.TabIndex = 4;
            this.LandedTxt.Text = "Landed: 0";
            // 
            // RedLanding
            // 
            this.RedLanding.BackColor = System.Drawing.Color.Transparent;
            this.RedLanding.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.RedLanding.Location = new System.Drawing.Point(72, 145);
            this.RedLanding.Name = "RedLanding";
            this.RedLanding.Size = new System.Drawing.Size(31, 15);
            this.RedLanding.TabIndex = 5;
            this.RedLanding.TabStop = false;
            this.RedLanding.Visible = false;
            // 
            // YellowLanding
            // 
            this.YellowLanding.BackColor = System.Drawing.Color.Transparent;
            this.YellowLanding.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.YellowLanding.Location = new System.Drawing.Point(601, 95);
            this.YellowLanding.Name = "YellowLanding";
            this.YellowLanding.Size = new System.Drawing.Size(31, 15);
            this.YellowLanding.TabIndex = 6;
            this.YellowLanding.TabStop = false;
            this.YellowLanding.Visible = false;
            // 
            // WhiteLanding
            // 
            this.WhiteLanding.BackColor = System.Drawing.Color.Transparent;
            this.WhiteLanding.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.WhiteLanding.Location = new System.Drawing.Point(797, 249);
            this.WhiteLanding.Name = "WhiteLanding";
            this.WhiteLanding.Size = new System.Drawing.Size(18, 20);
            this.WhiteLanding.TabIndex = 7;
            this.WhiteLanding.TabStop = false;
            this.WhiteLanding.Visible = false;
            // 
            // HPad
            // 
            this.HPad.BackColor = System.Drawing.Color.Transparent;
            this.HPad.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.HPad.Location = new System.Drawing.Point(837, 289);
            this.HPad.Name = "HPad";
            this.HPad.Size = new System.Drawing.Size(36, 34);
            this.HPad.TabIndex = 8;
            this.HPad.TabStop = false;
            this.HPad.Visible = false;
            // 
            // HPad2
            // 
            this.HPad2.BackColor = System.Drawing.Color.Transparent;
            this.HPad2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.HPad2.Location = new System.Drawing.Point(337, 69);
            this.HPad2.Name = "HPad2";
            this.HPad2.Size = new System.Drawing.Size(29, 31);
            this.HPad2.TabIndex = 9;
            this.HPad2.TabStop = false;
            this.HPad2.Visible = false;
            // 
            // Map2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::FlightControlGame.Properties.Resources.Map21;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.ClientSize = new System.Drawing.Size(984, 531);
            this.ControlBox = false;
            this.Controls.Add(this.HPad2);
            this.Controls.Add(this.HPad);
            this.Controls.Add(this.WhiteLanding);
            this.Controls.Add(this.YellowLanding);
            this.Controls.Add(this.RedLanding);
            this.Controls.Add(this.LandedTxt);
            this.Controls.Add(this.Menu);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Map2";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Map 2";
            this.Load += new System.EventHandler(this.Map2_Load);
            ((System.ComponentModel.ISupportInitialize)(this.RedLanding)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.YellowLanding)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.WhiteLanding)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.HPad)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.HPad2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button Menu;
        private System.Windows.Forms.Timer MoveObjects;
        private System.Windows.Forms.Timer SpawnAircraft;
        private System.Windows.Forms.Timer Crash;
        private System.Windows.Forms.Label LandedTxt;
        private System.Windows.Forms.PictureBox RedLanding;
        private System.Windows.Forms.PictureBox YellowLanding;
        private System.Windows.Forms.PictureBox WhiteLanding;
        private System.Windows.Forms.PictureBox HPad;
        private System.Windows.Forms.PictureBox HPad2;
    }
}