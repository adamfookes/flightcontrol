﻿namespace FlightControlGame
{
    partial class StatMenu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(StatMenu));
            this.MainBtn = new System.Windows.Forms.Button();
            this.OptionBtn = new System.Windows.Forms.Button();
            this.TopScoresText = new System.Windows.Forms.Label();
            this.NextBtn = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // MainBtn
            // 
            this.MainBtn.BackColor = System.Drawing.Color.Transparent;
            this.MainBtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.MainBtn.FlatAppearance.BorderSize = 0;
            this.MainBtn.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.MainBtn.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.MainBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.MainBtn.ForeColor = System.Drawing.Color.Transparent;
            this.MainBtn.Location = new System.Drawing.Point(851, 23);
            this.MainBtn.Name = "MainBtn";
            this.MainBtn.Size = new System.Drawing.Size(63, 95);
            this.MainBtn.TabIndex = 3;
            this.MainBtn.TabStop = false;
            this.MainBtn.UseVisualStyleBackColor = false;
            this.MainBtn.Click += new System.EventHandler(this.MainBtn_Click);
            // 
            // OptionBtn
            // 
            this.OptionBtn.BackColor = System.Drawing.Color.Transparent;
            this.OptionBtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.OptionBtn.FlatAppearance.BorderSize = 0;
            this.OptionBtn.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.OptionBtn.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.OptionBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.OptionBtn.ForeColor = System.Drawing.Color.Transparent;
            this.OptionBtn.Location = new System.Drawing.Point(851, 131);
            this.OptionBtn.Name = "OptionBtn";
            this.OptionBtn.Size = new System.Drawing.Size(63, 95);
            this.OptionBtn.TabIndex = 4;
            this.OptionBtn.TabStop = false;
            this.OptionBtn.UseVisualStyleBackColor = false;
            this.OptionBtn.Click += new System.EventHandler(this.OptionBtn_Click);
            // 
            // TopScoresText
            // 
            this.TopScoresText.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TopScoresText.BackColor = System.Drawing.Color.Transparent;
            this.TopScoresText.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TopScoresText.ForeColor = System.Drawing.Color.White;
            this.TopScoresText.Image = global::FlightControlGame.Properties.Resources.Chalk;
            this.TopScoresText.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.TopScoresText.Location = new System.Drawing.Point(200, 240);
            this.TopScoresText.Name = "TopScoresText";
            this.TopScoresText.Size = new System.Drawing.Size(224, 175);
            this.TopScoresText.TabIndex = 5;
            this.TopScoresText.Text = "label1";
            this.TopScoresText.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.TopScoresText.Click += new System.EventHandler(this.TopScoresText_Click);
            // 
            // NextBtn
            // 
            this.NextBtn.BackColor = System.Drawing.Color.Transparent;
            this.NextBtn.BackgroundImage = global::FlightControlGame.Properties.Resources.NextBtn;
            this.NextBtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.NextBtn.FlatAppearance.BorderSize = 0;
            this.NextBtn.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.NextBtn.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.NextBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.NextBtn.Location = new System.Drawing.Point(365, 192);
            this.NextBtn.Name = "NextBtn";
            this.NextBtn.Size = new System.Drawing.Size(70, 45);
            this.NextBtn.TabIndex = 6;
            this.NextBtn.TabStop = false;
            this.NextBtn.UseVisualStyleBackColor = false;
            this.NextBtn.Click += new System.EventHandler(this.NextBtn_Click);
            // 
            // StatMenu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::FlightControlGame.Properties.Resources.StatMenu1;
            this.ClientSize = new System.Drawing.Size(984, 531);
            this.ControlBox = false;
            this.Controls.Add(this.NextBtn);
            this.Controls.Add(this.TopScoresText);
            this.Controls.Add(this.OptionBtn);
            this.Controls.Add(this.MainBtn);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "StatMenu";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Stat Menu";
            this.Load += new System.EventHandler(this.StatMenu_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button MainBtn;
        private System.Windows.Forms.Button OptionBtn;
        private System.Windows.Forms.Label TopScoresText;
        private System.Windows.Forms.Button NextBtn;
    }
}