﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FlightControlGame
{
    public partial class OptionMenu : Form
    {
        public static bool CrashVol = false;
        public static bool LandVol = false;
        public static bool MusicVol = false;

        public static string Difficulty = "EASY";

        public OptionMenu()
        {
            InitializeComponent();
        }

        private void MainBtn_Click(object sender, EventArgs e)
        {
            this.Hide();
            MainMenu MM = new MainMenu();
            MM.ShowDialog();
        }

        private void StatBtn_Click(object sender, EventArgs e)
        {
            this.Hide();
            StatMenu SM = new StatMenu();
            SM.ShowDialog();
        }

        private void OptionMenu_Load(object sender, EventArgs e)
        {
            comboBox1.SelectedIndex = comboBox1.FindStringExact(Difficulty);

            if (MusicVol == true)
            {
                checkedListBox1.SetItemCheckState(0, CheckState.Checked);
            }
            if (CrashVol == true)
            {
                checkedListBox1.SetItemCheckState(1, CheckState.Checked);
            }
            if (LandVol == true)
            {
                checkedListBox1.SetItemCheckState(2, CheckState.Checked);
            }

            if (MusicVol == false)
            {
                checkedListBox1.SetItemCheckState(0, CheckState.Unchecked);
            }
            if (CrashVol == false)
            {
                checkedListBox1.SetItemCheckState(1, CheckState.Unchecked);
            }
            if (LandVol == false)
            {
                checkedListBox1.SetItemCheckState(2, CheckState.Unchecked);
            }
        }

        private void checkedListBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (checkedListBox1.GetItemChecked(0))
            {
                MusicVol = true;
                Console.WriteLine("Music");
            }
            else if (checkedListBox1.GetItemChecked(1))
            {
                CrashVol = true;
                Console.WriteLine("Crash");
            }
            else if (checkedListBox1.GetItemChecked(2))
            {
                LandVol = true;
                Console.WriteLine("Land");
            }
            else
            {
                //Will Need To Fix When Other Sounds Are Addded
                MusicVol = false;
                CrashVol = false;
                LandVol = false;
            }
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBox1.SelectedIndex == 0)
            {
                Difficulty = "EASY";
            }
            if (comboBox1.SelectedIndex == 1)
            {
                Difficulty = "INTERMEDIATE";
            }
            if (comboBox1.SelectedIndex == 2)
            {
                Difficulty = "HARD";
            }
        }
    }
}
